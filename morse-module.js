
let codes = new Map();
codes.set(" ","/");
// numbers
codes.set("0","-----");
codes.set("1",".----");
codes.set("2","..---");
codes.set("3","...--");
codes.set("4","....-");
codes.set("5",".....");
codes.set("6","-....");
codes.set("7","--...");
codes.set("8","---..");
codes.set("9","----.");
// letters
codes.set("A", ".-");
codes.set("B", "-...");
codes.set("C", "-.-.");
codes.set("D", "-..");
codes.set("E", ".");
codes.set("F", "..-.");
codes.set("G", "--.");
codes.set("H", "....");
codes.set("I", "..");
codes.set("J", ".---");
codes.set("K", "-.-");
codes.set("L", ".-..");
codes.set("M", "--");
codes.set("N", "-.");
codes.set("O", "---");
codes.set("P", ".--.");
codes.set("Q", "--.-");
codes.set("R", ".-.");
codes.set("S", "...");
codes.set("T", "-");
codes.set("U", "..-");
codes.set("W", ".--");
codes.set("X", "-..-");
codes.set("Y", "-.--");
codes.set("Z", "--..");

/*
* encode a text to morse code.
*/
function encodeTextToMorse(text) {
    let morseText = "";
    let letterList = text.toUpperCase().split("");
    letterList.map(value => {
        return (codes.get(value)!==undefined?morseText += ` ${codes.get(value)}`:morseText += ` ${value}`)
    });
    return morseText;
}


/*
* decode a morse code to text.
*/
function decodeMorseToText(morse) {
    let decoded = "";
    let morseList = morse.split(" ");
    morseList.forEach(value => {
        decoded += getKey(value);
    })
    return decoded;
}

/*
* method used on decode method to get key of a map
*/
function getKey(value) {
    return [...codes].find(([key, val]) => val === value)[0];
}


module.exports.encodeToMorse = encodeTextToMorse;
module.exports.decodeToText = decodeMorseToText;
